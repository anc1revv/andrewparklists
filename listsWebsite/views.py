from django.shortcuts import render
from django.http import HttpResponse
from django.template import RequestContext

from listsWebsite.models import Posting

def homepage(request):
    all_posts = Posting.objects.all().order_by('-submitted_date')

    context = {'Postings':all_posts, 'request':request}
    return render(request, 'listsWebsite/homepage.html', context, context_instance=RequestContext(request))

def homepage2(request):
	return HttpResponse("hi")