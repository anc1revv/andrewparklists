from django.db import models

# Create your models here.
class Posting(models.Model):
	title = models.CharField(max_length=200, blank=True, null=True)
	post = models.TextField()
	submitted_date = models.DateTimeField(auto_now_add=True)
	tags = models.CharField(max_length=200, blank=True, null=True)

	def __unicode__(self):
		return self.title